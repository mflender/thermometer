A small thermometer, barometer, hygrometer, embedded system.
The board layout is made with [Fritzing](http://fritzing.org/home/).
It uses an [ATmega328P](http://www.atmel.com/images/Atmel-8271-8-bit-AVR-Microcontroller-ATmega48A-48PA-88A-88PA-168A-168PA-328-328P_datasheet_Complete.pdf)
with the [arduino](https://www.arduino.cc/) framework.

Display: [SHARP Memory Display Breakout - 1.3" 96x96 Silver Monochrome](https://www.adafruit.com/products/1393)  
Barometric sensor: [BMP180 Barometric Pressure/Temperature/Altitude Sensor- 5V ready](https://www.adafruit.com/products/1603)  
Temperature sensor: [MCP9808 High Accuracy I2C Temperature Sensor Breakout Board](https://www.adafruit.com/products/1782)  
Humidity sensor: [Adafruit HTU21D-F Temperature & Humidity Sensor Breakout Board](https://www.adafruit.com/products/1899)  

At the moment this circuit needs ~540µA:  
17µA for the BMP180  
16µA for the HTU21D  
184µA for the MCP9808  
18µA for the Display  
~300µA are missing  

LICENSE of the Implementation is: [GPL-2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)