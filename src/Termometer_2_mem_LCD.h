/*
 * Copyright 2014 Malte
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _Termometer_2_mem_LCD_H_
#define _Termometer_2_mem_LCD_H_

#include "Arduino.h"

#ifdef __cplusplus
extern "C"
{
#endif
void loop();
void setup();
#ifdef __cplusplus
} // extern "C"
#endif

/**
 * the pins on which the display is attached
 */
#define SCK 10
#define MOSI 11
#define SS 13

/**
 * The two states every pixel in the display can have.
 */
#define BLACK 0
#define WHITE 1

/**
 * Change the values on the display if they changed more than this.
 */
#define AIR_PRESSURE_DIFFERENCE 1.0
#define TEMPERATURE_DIFFERENCE 0.5
#define HUMIDITY_DIFFERENCE 1.0

/**
 * Multiplied by 8 seconds this is the
 * time in witch the display is updated.
 */
#define COUNT_FACTOR 100

void enter_sleep(void);
void get_temperature(float *temperature);
void get_temperature_all(float *temperature);
void get_humidity(float *humidity);
void get_air_pressure(float *air_pressure);
void update_display(void);
void save_power();

#endif /* _Termometer_2_mem_LCD_H_ */
