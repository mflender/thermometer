/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Malte Flender
 * @date 16.08.2014
 * @file Termometer_2_mem_LCD.cpp
 * @brief A small thermometer using arduino uno,
 * BMP180,MCP9808,HTU21D sensor and a SHARP Memory LCD Display,
 * temperature is measured in C°,
 * air pressure is measured in hPa,
 * humidity is measured in %.
 *
 *
 * The Pins are:
 *
 *	Arduino:
 *		A4 -> I2C SDA
 *		A5 -> I2C SCL
 *		10 -> CLK Display
 *		11 -> DI Display
 *		13 -> CS Display
 *
 * At the moment this circuit needs ~540µA:
 * 	17µA for the BMP180
 * 	16µA for the HTU21D
 * 	184µA for the MCP9808
 * 	18µA for the Display
 * 	~300µA are missing
 *
 */

#include "Termometer_2_mem_LCD.h"

/**
 * all you need to set the controller to
 * sleep so you can save some power.
 */
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <avr/power.h>

/**
 * BMP180: Barometric Pressure/Temperature/Altitude Sensor.
 */
#include <Adafruit_BMP085_U.h>
#include <Adafruit_Sensor.h>

/**
 * MCP9808: High Accuracy I2C Temperature Sensor.
 */
#include <Adafruit_MCP9808.h>

/**
 * HTU21D-F: Temperature & Humidity Sensor.
 */
#include <Adafruit_HTU21DF.h>

/**
 * SHARP Memory Display 1.3" 96x96 Silver Monochrome
 */
#include <Adafruit_GFX.h>
#include <Adafruit_SharpMem.h>

/**
 * Indicates that a interrupt has occurred.
 */
volatile bool watchdog_interrupt = true;

Adafruit_SharpMem display = Adafruit_SharpMem(SCK, MOSI, SS);
Adafruit_BMP085_Unified bmp180 = Adafruit_BMP085_Unified(1);
Adafruit_MCP9808 mcp9808 = Adafruit_MCP9808();
Adafruit_HTU21DF htu21d = Adafruit_HTU21DF();

/**
 * Start the display, sensors and the watchdog.
 */
void setup()
{
	save_power();

	//init display
	display.begin();
	display.clearDisplay();
	display.setTextSize(4);
	display.setTextColor(BLACK);
	display.setCursor(0, 0);
	display.setTextWrap(false);

	//init BMP180
	if (!bmp180.begin()) {
		while (1) {
			//Don't print a full error message(the display is to small).
			display.println("BMP180");
			display.refresh();
			delay(1000);
		}
	}

	//init MCP9808
	if (!mcp9808.begin()) {
		while (1) {
			display.println("MCP9808");
			display.refresh();
			delay(1000);
		}
	}

	//init HTU21DF
	if (!htu21d.begin()) {
		while (1) {
			display.println("HTU21DF");
			display.refresh();
			delay(1000);
		}
	}

	//init watchdog timer
	MCUSR &= ~(1 << WDRF);
	WDTCSR |= (1 << WDCE) | (1 << WDE);
	WDTCSR = 1 << WDP0 | 1 << WDP3;
	WDTCSR |= _BV(WDIE);

	update_display();
}

/**
 * Runs over and over again if there was
 * an interrupt update the display and sleep again.
 */
void loop()
{
	if (watchdog_interrupt == true) {
		watchdog_interrupt = false;
		update_display();
	}
	enter_sleep();
}

/**
 * Goes to sleep mode to save power.
 */
void enter_sleep(void)
{
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_enable();
	sleep_mode();
	sleep_disable();
}

/**
 * Check if the vales are out of the +-difference range
 * and updates the display with the new values.
 */
void update_display(void)
{
	float air_pressure, temperature, humidity;
	static float air_pressure_old, temperature_old, humidity_old;

	get_air_pressure(&air_pressure);
	get_temperature_all(&temperature);
	get_humidity(&humidity);

	if ((air_pressure >= air_pressure_old + AIR_PRESSURE_DIFFERENCE)
			|| (air_pressure <= air_pressure_old - AIR_PRESSURE_DIFFERENCE)
			|| (temperature >= temperature_old + TEMPERATURE_DIFFERENCE)
			|| (temperature <= temperature_old - TEMPERATURE_DIFFERENCE)
			|| (humidity >= humidity_old + HUMIDITY_DIFFERENCE)
			|| (humidity <= humidity_old - HUMIDITY_DIFFERENCE)) {

		air_pressure_old = air_pressure;
		temperature_old = temperature;
		humidity_old = humidity;

		display.clearDisplay();
		display.setCursor(0, 0);
		display.println(temperature);
		display.println((int) air_pressure);
		display.println(humidity);
		display.refresh();
	}
}

/**
 * Reads the air pressure from the BMP180 sensor.
 *
 * @param air_pressure [out]
 * the measured value is stored in here
 */
void get_air_pressure(float *air_pressure)
{
	sensors_event_t event;

	bmp180.getEvent(&event);
	*air_pressure = event.pressure;
}

/**
 * Reads the temperature from the MCP9808 sensor.
 *
 * @param temperature [out]
 * the measured value is stored in here
 */
void get_temperature(float *temperature)
{
	*temperature = mcp9808.readTempC();
}

/**
 * Reads the temperature from all three sensors
 * and computes the average.
 *
 * @param temp [out]
 * the measured values are stored in here
 */
void get_temperature_all(float *temperature)
{
	float bmp180_tmp, mcp9808_tmp, htu21d_tmp;

	mcp9808_tmp = mcp9808.readTempC();
	htu21d_tmp = htu21d.readTemperature();
	bmp180.getTemperature(&bmp180_tmp);

	*temperature = ((bmp180_tmp + mcp9808_tmp + htu21d_tmp) / 3);
}

/**
 * Reads the humidity from the HTU21DF sensor.
 *
 * @param humidity [out]
 * the measured value is stored in here
 */
void get_humidity(float *humidity)
{
	*humidity = htu21d.readHumidity();
}

/**
 * Counts to COUNT_FACTOR and then propagates an interrupt.
 */
ISR(WDT_vect)
{
	static int i = 0;
	if (watchdog_interrupt == false && i >= COUNT_FACTOR) {
		watchdog_interrupt = true;
		i = 0;
	} else {
		i++;
	}
}

/**
 * Save as much power as possible.
 */
void save_power()
{
	power_adc_disable();
	power_spi_disable();
	power_usart0_disable();
	power_timer1_disable();
	power_timer2_disable();

	/**
	 * Disable ADC, analog comparator and all digital input buffers.
	 */
	ADCSRA &= ~(1 << ADEN);
	ACSR = (1 << ACD);
	DIDR0 = 0x3F;
	DIDR1 = (1 << AIN1D) | (1 << AIN0D);

	/**
	 * Dived the controller speed by 1 to 256,
	 * but the watchdog timer runs at the same speed.
	 * So i don't think it will save any power at all.
	 */
	//clock_prescale_set(clock_div_16);
}
